import os
import logging
from os import listdir
from os.path import isfile, join
from requests import get
from telethon import TelegramClient, events, sync


api_id = os.environ["API_ID"]
api_hash = os.environ["API_HASH"]

client = TelegramClient("session", api_id, api_hash)
client.start()


CHANNEL_ID = os.environ["CHANNEL_ID"]

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)


build_path = "./dist"
tempfiles = [f for f in listdir(build_path) if isfile(join(build_path, f))]

build_files = []
for file in tempfiles:
    if file.endswith(".exe") or file.endswith(".dmg") or file.endswith(".AppImage"):
        build_files.append(file)
if not len(build_files):
    logging.fatal("No binaries found.")
build_files = sorted(build_files)

COMMIT_ID = os.environ["BUILD_SOURCEVERSION"]

url = f"https://notabug.org/RemixDevs/DeezloaderRemix/commit/{COMMIT_ID}"

# getting commit title and desc
r = get(url)
if 'h3' in r.text:
    info = r.text[r.text.find("<h3>")+4:]
    info = info[:info.find("</h3>")]
else:
    info = r.text[r.text.find("<h4>")+4:]
    info = info[:info.find("</h4>")]
info = info.replace('<br>', '\n')

client.parse_mode = "html"
client.send_message(CHANNEL_ID, f"Commit <a href={url}>{COMMIT_ID[:10]}</a>\n{info}", link_preview=False)

for file in build_files:
    logging.info("Uploading " + file + "...")
    curr_build_path = join(build_path, file)
    client.send_file(CHANNEL_ID, curr_build_path)
