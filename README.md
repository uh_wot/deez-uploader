# deez-uploader

A simple userbot for uploading Deezloader Remix binaries on Telegram channels or groups.

Made for [@DeezloaderRemixBeta](https://t.me/DeezloaderRemixBeta).

# how 2 setup

- Set these environment variables:

    - `API_ID`: Your API ID from https://my.telegram.org/.
    - `API_HASH`: Your API secret from https://my.telegram.org/.
    - `CHANNEL_ID`: The Telegram channel or group username.
    - `BUILD_SOURCEVERSION`: The commit ID.

- Install the dependencies:

    - `pip3 install -r requirements.txt`

# how 2 run

- Run: `python3 deez-uploader.py`

# initial setup

When you first run the bot, it will ask for a phone number, then it will send a code to the Telegram account that you have to enter.

After this, it will create a session file.